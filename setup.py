from distutils.core import setup, Extension
from distutils.command import sdist
from os import popen

class Sdist(sdist.sdist):
    default_format = {'posix': 'bztar'}

setup(name='pyghosd',
      version='0.0.1',
      description='Crude ghosd text hack',
      author='Sylvain Fourmanoit',
      author_email='syfou@users.berlios.de',
      license='BSD', 
      ext_modules=[Extension('_ghosd',
                             sources=['ghosd.c'],
                             libraries = ['X11'],
                             extra_compile_args = 
                             popen("pkg-config --cflags ghosd").read().split(),
                             extra_link_args = 
                             popen("pkg-config --libs ghosd").read().split()
                             )
                   
                   ],
      py_modules=['ghosd'],
      cmdclass = {'sdist' : Sdist}
      )
