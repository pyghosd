"""
ghosd for Python

Right now, this does not contain bindings for ghosd, but a simple hack
to bring parametrable ghosd'ed text display facility to Python. It
doesn't rely on any high-level Python bindings (PyGTK, Pycairo, ...)
either. Usage is pretty simple:

>>> from ghosd import flash_text
>>> flash_text(\"<span font_desc='Trebuchet 30'>Hello, world!</span>\")
"""
# We don't really need this frontend right now, but who knows...
#
from _ghosd import *

# An example? All right: here is a copycat of ghosd-0.0.1/examples/text.c
#
if __name__ == '__main__':
    # It doesn't get easier than that. ;-D
    #
    flash_text("<span font_desc='Trebuchet 30'>"
               "some sample text using <b>pyghosd</b>"
               "</span>")
