/* Fast refactoring of ghosd-0.0.1/examples/text.c.
 */

#include <Python.h>

#include <cairo/cairo.h>
#include <pango/pangocairo.h>
#include <time.h>
#include <unistd.h>

#include <X11/Xlib.h>

#include <ghosd/ghosd.h>
#include <ghosd/ghosd-text.h>

struct _ghosd_render_info {
  PangoLayout * layout;
  float fgr, fgg, fgb, fga,
        bgr, bgg, bgb, bga;
};

static void 
render(Ghosd *ghosd, cairo_t *cr, void* data) {
  
  struct _ghosd_render_info * ri = data;

  /* drop shadow! */
  cairo_set_source_rgba(cr, ri->bgr, ri->bgg, ri->bgb, ri->bga);
  cairo_move_to(cr, 2, 2);
  pango_cairo_show_layout(cr, ri->layout);

  /* and the actual text. */
  cairo_set_source_rgba(cr, ri->fgr, ri->fgg, ri->fgb, ri->fga);
  cairo_move_to(cr, 0, 0);
  pango_cairo_show_layout(cr, ri->layout);
}

static PyObject *
ghosd_flash_text(PyObject * self, PyObject * args, PyObject * kw)
{
  static char * kwlist[] = {"text", "x", "y", 
			    "fade_time", "display_time", 
			    "fg_color", "bg_color", NULL};
  Ghosd *ghosd;
  PangoContext * context;
  struct _ghosd_render_info ri;
  int x = -50, y = -50, fade = 300, dtime = 2000;
  char * text = NULL; 
  PyObject * fg_color = NULL, * bg_color = NULL;
    
  if (!PyArg_ParseTupleAndKeywords(args, kw, "s|iiiiOO", 
				   kwlist, 
				   &text,
                                   &x, &y, &fade, &dtime, &fg_color, &bg_color))
    return NULL;

#define cbcheck(nam, c) ((ri.nam ## c >= 0) && (ri.nam ## c <= 1))
#define colcheck(nam)  \
  if (nam ## _color) { \
    if (!PyTuple_Check(nam ## _color)) { \
      PyErr_SetString(PyExc_TypeError, #nam "_color should be a tuple"); \
      return NULL; \
    } \
    if (!PyArg_ParseTuple(nam ## _color, "ffff",  \
			  & ri.nam ## r, & ri.nam ## g, \
			  & ri.nam ## b, & ri.nam ## a)) {	\
      PyErr_SetString(PyExc_TypeError, \
		      #nam "_color should be a 4-tuple of floats"); \
      return NULL; \
    } \
    if (!cbcheck(nam, r) || !cbcheck(nam, g) || \
	!cbcheck(nam, b) || !cbcheck(nam, a)) {	\
      PyErr_SetString(PyExc_ValueError, \
		      "all components in " #nam \
		      "_color should be between 0 and 1"); \
      return NULL; \
    } \
  }

  ri.fgr = ri.fgg = ri.fgb = ri.fga = 1;
  ri.bgr = ri.bgg = ri.bgb          = 0; ri.bga = .5;

  colcheck(fg)
  colcheck(bg)

#undef cbcheck
#undef colcheck

  /* Finally, we are there: let's use this ghosd lib!
   */
  if (!(context = pango_cairo_font_map_create_context(
		  PANGO_CAIRO_FONT_MAP(pango_cairo_font_map_get_default())))) {
    PyErr_SetString(PyExc_RuntimeError,				  
		    "could not create pango context");
    return NULL;
  }
  
  if (!(ri.layout = pango_layout_new(context))) {
    PyErr_SetString(PyExc_RuntimeError,				  
		    "could not create pango layout");
    g_object_unref(context);
    return NULL;
  }

  pango_layout_set_markup(ri.layout, text, -1);

  if (!(ghosd = ghosd_new())) {
    PyErr_SetString(PyExc_RuntimeError,				  
		    "could not create ghosd object");
    g_object_unref(ri.layout);
    g_object_unref(context);
    return NULL;
  }

  ghosd_text_set_position(ghosd, x, y, ri.layout);
  ghosd_set_render(ghosd, render, &ri);

  ghosd_flash(ghosd, fade, dtime);

  /* Free the various objects */
  free(ghosd);                /* See ghosd-0.0.1/ghosd/ghosd.c:
			         no API for now, but that's just 
			         a calloc'ed struct: no API to close
			         the display unfortunately. */
  g_object_unref(ri.layout);
  g_object_unref(context);

  Py_INCREF(Py_None);
  return Py_None;
  
}

static PyMethodDef ghosdMethods[] = {
  { "flash_text", (PyCFunction)ghosd_flash_text, METH_VARARGS | METH_KEYWORDS ,
"flash_text(text, x=-50, y=-50, fade_time=300, display_time=2000, \n\
fg_color=(1, 1, 1, 1), bg_color=(0, 0, 0, .5)) => None\n\
\n\
Flash a given text message onto the screen.\n\
\n\
- text         text to be displayed, in Pango markup (see below)\n\
- x, y         position onto the screen\n\
- fade_time    time, in milliseconds, for fading the text in and out\n\
- display_time time the text is to be displayed\n\
- fg_color     RGBA default text color\n\
- bg_color     RGBA text shadow color\n\
\n\
A concise description of the Pango Markup Language can be found\n\
in PyGTK documentation:\n\
\n\
http://www.pygtk.org/docs/pygtk/pango-markup-language.html\n\
"},
  {NULL}
};


PyMODINIT_FUNC
init_ghosd(void)
{
  g_type_init();
  Py_InitModule("_ghosd", ghosdMethods);
}
